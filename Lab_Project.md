#include <iostream>

using namespace std;

int main()
{
    int n,r,c;
    cin>>n;// розмір масиву
    int mat [n*1][n*1];    // оголошення матриці
    for (int i=0; i<n; i++)  
    for (int j=0;j<n; j++)
    mat[i][j]=random();
    
    cin>>r>>c;
    
    for (int i=0; i<r; i++){
    for (int j=0;j<c; j++){
    cout<<mat [i][j]<<'\t';
    }
    cout<<'\n';
    }
    return 0;
}
